
import {ClientTable} from 'vue-tables-2';
import Vue from 'vue'
import App from './App.vue'
import TransactionBalance from './Components/TransactionBalance.vue'
import TransactionForm from './Components/TransactionForm.vue'
import TransactionTable from './Components/TransactionTable.vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueSweetalert2 from 'vue-sweetalert2';

Vue.component('TransactionBalance',TransactionBalance)
Vue.component('TransactionForm',TransactionForm)
Vue.component('TransactionTable',TransactionTable)

let options = {};
let useVuex = false;
let theme = "bootstrap4";
let template = "default";

new Vue({
  el: '#app',
  render: h => h(App)
})
Vue.use(BootstrapVue,ClientTable,options, useVuex, theme, template,VueAxios, axios,VueSweetalert2);

